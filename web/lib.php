<?php

class Lib
{
    private $dbh;

    public function __construct()
    {
        $opt = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];

        $this->dbh = new PDO('mysql:host=127.0.0.1;dbname=ms_ns;charset=utf8', 'root', '', $opt);
    }

    public function getCounts() : array
    {
        $sql = "SELECT max(lvl) FROM ".$this->getTableName();
        $sql2 = "SELECT count(lvl) as ln, lvl FROM ".$this->getTableName() ." GROUP BY lvl ORDER BY `lvl`";


        $ret = [
            'maxLevels' => $this->dbh->query($sql)->fetch(PDO::FETCH_NUM)[0],
            'countsLevels' => []
        ];

        array_map(function($item) use (&$ret) {
            $ret['countsLevels'][$item['lvl']] = $item['ln'];
        }, $this->dbh->query($sql2)->fetchAll());

        return $ret;

    }

    public function getNodes(int $level = 1) : array
    {
        $sql = "SELECT t.*, (SELECT count(*) FROM ".$this->getTableName()
            ." `t2` WHERE `t2`.`lft` > `t`.`lft` AND `t2`.`rgt` < `t`.`rgt` AND `t2`.`tree_root` = `t`.`tree_root`) as childLen FROM ".$this->getTableName() ." t WHERE `t`.`lvl` = $level ORDER BY `t`.`lvl`, `t`.`user_id`";

        $sth = $this->dbh->query($sql);
        return $sth->fetchAll();
    }

    public function getNodesNs() : array
    {
        $sql = "SELECT t.*,(SELECT count(*) FROM ".$this->getTableName()
            ." `t2` WHERE `t2`.`lft` > `t`.`lft` AND `t2`.`rgt` < `t`.`rgt` AND `t2`.`tree_root` = `t`.`tree_root`) as childLen FROM ".$this->getTableName() ." t ORDER BY `t`.`lft`";

        $sth = $this->dbh->query($sql);
        return $sth->fetchAll();
    }


    public function getRecursiveData(int $parentId) : array
    {
        $sql = "SELECT t.*  FROM ".$this->getTableName() ." t WHERE t.parent_id ".(!empty($parentId) ? ' = '. $parentId : 'IS NULL');
        $sth = $this->dbh->query($sql);
        return $sth->fetchAll();
    }


    public function printRecursive(int $parentId) : string
    {
        $items = $this->getRecursiveData($parentId);
        $ret = '';
        if (!empty($items)) {
            $ret .= '<table  style="border: 0;" align="left"  width="100%"><tr>';
            foreach ($items as $item) {

                $children = $this->printRecursive($item['user_id']);


                $ret .= '<td align="center" width="'.(100 / count($items)).'%" valign="top"><table width="100%" style="border: 0;"><tr><td align="center" style="border: 0;">
                        
                        <div style="border: 1px solid forestgreen; width: 165px; padding: 20px; margin-top: 10px;">l: '.$item['lft'].' ( user: '. $item['user_id'] .') r: '.$item['rgt'].'</div>
                        
                        
                        </td></tr>';
                if (!empty((100 / count($items)))) {

                    $ret .= '<tr><td>'.$children.'</td></tr>';
                }

                $ret .= '</table></td>';


            }

            $ret .= '</tr>';
            $ret .= '</table>';

        }
        return $ret;

    }

    public function getTableName() : string
    {
        return 'un_';
    }
}
