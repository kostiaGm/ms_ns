-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Июл 09 2019 г., 09:34
-- Версия сервера: 10.1.40-MariaDB-0ubuntu0.18.04.1
-- Версия PHP: 7.2.19-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `ms_ns`
--

-- --------------------------------------------------------

--
-- Структура таблицы `un_`
--

CREATE TABLE `un_` (
  `id` int(11) NOT NULL,
  `tree_root` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `lft` int(11) NOT NULL,
  `rgt` int(11) NOT NULL,
  `lvl` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `un_`
--
ALTER TABLE `un_`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_A52E4C79A977936C` (`tree_root`),
  ADD KEY `IDX_A52E4C79727ACA70` (`parent_id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `un_`
--
ALTER TABLE `un_`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteNode`(IN userId INTEGER)
BEGIN
	
	DECLARE userLft, userRgt, userRoot, firstRgt, firstRoot  INT DEFAULT 0;      
    START TRANSACTION;
    
    SELECT lft, rgt, tree_root INTO @userLft, @userRgt, @userRoot from un_ WHERE user_id=userId;    
    SELECT rgt, tree_root INTO @firstRgt, @firstRoot from un_ WHERE tree_root=@userRoot AND lft=1;        
    
    DELETE FROM `un_` WHERE user_id=userId;
    # Вычитаем 1 от левого и правого ключа у дочерних элементов и здвигаем все дочерние элементы на уровень вверх    
    UPDATE `un_` SET lft = lft - 1, rgt = rgt - 1, lvl = lvl - 1, parent_id = (CASE WHEN parent_id = userId THEN parent_id - 1 ELSE parent_id END) WHERE lft > @userLft AND rgt < @userRgt AND tree_root=@userRoot; 
    
    # Вычитаем 2 от правого ключа у родительских элементов
    UPDATE `un_` SET rgt = rgt - 2 WHERE lft < @userLft AND rgt > @userRgt AND tree_root=@userRoot; 
    
    # Вычитаем 2 от левого и правого ключа во всех правых элеметах    
    UPDATE `un_` SET lft = lft - 2, rgt = rgt - 2 WHERE lft > @userLft AND rgt > @userRgt AND tree_root=@userRoot; 
    
    
    COMMIT;
	
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `createNode`(IN userId INTEGER, IN parentId INTEGER)
BEGIN
	DECLARE countTree, userLft, userRgt, userRoot  INT DEFAULT 0;      
    START TRANSACTION;
	IF parentId IS NOT NULL THEN    
		INSERT INTO `un_` (`id`, `tree_root`, `parent_id`, `user_id`, `lft`, `rgt`, `lvl`) SELECT NULL, `tree_root`, `user_id`, userId, `rgt`, `rgt`+1, `lvl`+1 FROM un_ WHERE user_id=parentId;        
        SELECT lft, rgt, tree_root INTO @userLft, @userRgt, @userRoot from un_ WHERE user_id=userId;
        # Меняем
        UPDATE un_ SET rgt = rgt+2 WHERE lft < @userLft AND rgt >= @userLft AND tree_root=@userRoot;      
        UPDATE un_ SET lft = lft + 2, rgt = rgt+2 WHERE lft > @userLft AND rgt > @userLft AND tree_root=@userRoot;      
    ELSE		
		SELECT max(`tree_root`) INTO @countTree FROM `un_`;
		INSERT INTO `un_` (`id`, `tree_root`, `parent_id`, `user_id`, `lft`, `rgt`, `lvl`) VALUES (NULL, (CASE WHEN @countTree IS NULL THEN 1 else @countTree + 1 END), NULL, userId, 1, 2, 1);
    END IF;  
    COMMIT;
	
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `deleteNodeWithChildren`(IN userId INTEGER)
BEGIN
	
	DECLARE userLft, userRgt, userRoot, firstRgt, firstRoot, childCount  INT DEFAULT 0;      
    START TRANSACTION;
    
    SELECT lft, rgt, tree_root INTO @userLft, @userRgt, @userRoot FROM un_ WHERE user_id=userId;    
    SELECT count(*) INTO @childCount FROM un_ WHERE lft > @userLft AND rgt < @userRgt AND tree_root = @userRoot;    
    
    #SELECT rgt, tree_root INTO @firstRgt, @firstRoot FROM un_ WHERE tree_root=@userRoot AND lft=1;        
    
    DELETE FROM `un_` WHERE lft >= @userLft AND rgt <= @userRgt AND tree_root = @userRoot;
    # Вычитаем 1 от левого и правого ключа у дочерних элементов и здвигаем все дочерние элементы на уровень вверх    
    UPDATE `un_` SET lft = lft - ((@childCount + 1) * 2), rgt = rgt - ((@childCount + 1) * 2)  WHERE lft > @userLft AND rgt > @userRgt AND tree_root=@userRoot; 
    UPDATE `un_` SET rgt = rgt - ((@childCount + 1) * 2) WHERE lft < @userLft AND rgt > @userRgt AND tree_root=@userRoot; 
    
    COMMIT;
	
END$$
DELIMITER ;

