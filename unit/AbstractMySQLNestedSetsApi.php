<?php


namespace MySQLNestedSetsApiTest\unit;


use PHPUnit\Framework\TestCase;
use \PDO;


abstract class AbstractMySQLNestedSetsApi extends TestCase
{

    protected static $dbh;

    //------------------------------ Public ----------------------------------------------------------------------------

    public function getRowDataByUserId(int $userId) : array
    {
        $stmt = self::$dbh->prepare("SELECT * FROM ".$this->getTableName()." WHERE user_id=?");
        $stmt->execute([$userId]);
        return  $stmt->fetch(PDO::FETCH_NUM);
    }

    //------------------------------ Public Static ---------------------------------------------------------------------

    public static function setUpBeforeClass(): void
    {
        $opt = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];
        self::$dbh = new PDO('mysql:host=127.0.0.1;dbname=ms_ns;charset=utf8', 'root', '', $opt);

    }

    public static function tearDownAfterClass(): void
    {
        self::$dbh = null;
    }

    public function clearData() : void
    {
        $this->executeSql("TRUNCATE ".$this->getTableName());
    }

    public function executeSql(string $sql) : void
    {

        $stmt = self::$dbh->prepare($sql);

        try {
            $stmt->execute();
        } catch (\Exception $e) {
            throw  $e;
        }

    }

    /**
     * [1, 2, 3, 4, 5, 6, 7]
     * 1 - id PK, 2 - tree_root, 3 - parent_id, 4 - user_id, 5 - lft, 6 - rgt, 7 - lvl
     * @return array
     */


    /**
     *                  1      (  1 )     20
     *                 /             /            \
     *            2 ( 2 ) 7      8 ( 5 ) 9   10 ( 6 ) 19
     *              /                        /        \
     *         3 ( 3 ) 6              11 ( 7 ) 14    15 ( 8 ) 18
     *            /                       /                \
     *       4 ( 4 ) 5             12  ( 10 ) 13          16 ( 9 ) 17
     */

    public function getDataProvider(): array
    {
        return [
            'user 1' => [1, 1, 0, 1, 1, 20, 1],
            'user 2' => [2, 1, 1, 2, 2, 7, 2],
            'user 3' => [3, 1, 2, 3, 3, 6, 3],
            'user 4' => [4, 1, 3, 4, 4, 5, 4],
            'user 5' => [5, 1, 1, 5, 8, 9, 2],
            'user 6' => [6, 1, 1, 6, 10, 19, 2],
            'user 7' => [7, 1, 6, 7, 11, 14, 3],
            'user 8' => [8, 1, 6, 8, 15, 18, 3],
            'user 9' => [9, 1, 8, 9, 16, 17, 4],
            'user 10' => [10, 1, 7, 10, 12, 13, 4]
        ];
    }

    //------------------------------ Public Abstract -------------------------------------------------------------------

    abstract public function getTableName() : string;

    //------------------------------ Protected -------------------------------------------------------------------------

    protected function diffArrayDataProviderAndDbTable(array $arrayDataProvider, string $message = '') : void
    {
        $stmt = self::$dbh->query("SELECT * FROM ".$this->getTableName());
        $items = $stmt->fetchAll(PDO::FETCH_NUM);
      //  $this->assertEquals(count($items), count($arrayDataProvider));

        $i = 0;
        foreach ($arrayDataProvider as $user => $row) {
            $this->assertArrayHasKey($i, $items);
            $item = (isset($items[$i]) ? $items[$i] : []);

            if (empty($item[2])) {
                $item[2] = 0;
            }

            foreach ($row as $f => $r) {
                $this->assertEquals($item[$f], $row[$f], "$message $user \n  item from database [$f]: $item[$f] item from assert array[$f]: $r \n");
            }
            $i++;
        }
    }

}