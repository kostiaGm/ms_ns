<?php


namespace MySQLNestedSetsApiTest\unit;


abstract class AbstractDeleteTest  extends AbstractMySQLNestedSetsApi
{
    use TraitBaseSetUp;

    public function getTableName(): string
    {
        return 'un_';
    }

    public function testDelete() : void
    {
        $userId = $this->getUserId();

        try {
            $this->executeSql("call deleteNode($userId)");
            $this->assertTrue(true);
        } catch (\Exception $e) {
            $this->assertTrue(false, $e->getMessage());
        }


        $this->diffArrayDataProviderAndDbTable($this->getUserSchema(), "Schema Id: $userId");
    }

    abstract protected function getUserId() : int;
    abstract protected function getUserSchema() : array;

}