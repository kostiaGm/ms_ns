<?php
/**
 * run this test
 * php vendor/bin/phpunit unit/InsertNodeUnderUserId3Test.php
 */

namespace MySQLNestedSetsApiTest\unit;


class InsertNodeUnderUserId3Test extends AbstractInsertTest
{
    /**
     * [1, 2, 3, 4, 5, 6, 7]
     * 1 - id PK, 2 - tree_root, 3 - parent_id, 4 - user_id, 5 - lft, 6 - rgt, 7 - lvl
     * @return array
     */


    /**
     *  -------------------- Before insert
     *
     *                  1      (  1 )     20
     *                 /             /            \
     *            2 ( 2 ) 7      8 ( 5 ) 9   10 ( 6 ) 19
     *              /                        /        \
     *         3 ( 3 ) 6              11 ( 7 ) 14    15 ( 8 ) 18
     *            /                       /                \
     *       4 ( 4 ) 5             12  ( 10 ) 13          16 ( 9 ) 17
     */

    /**
     *  -------------------- After inserted
     *
     *                  1      (  1 )     22
     *                 /             /            \
     *            2 ( 2 ) 9      10 ( 5 ) 11   12 ( 6 ) 21
     *              /                            /        \
     *         3 ( 3 ) 8                    13 ( 7 ) 16    17 ( 8 ) 20
     *            /                         /                \
     *  4 ( 4 ) 5  6 ( 11 new ) 7       14 ( 10 ) 15          18 ( 9 ) 19
     *
     *
     *
     */


    protected function getParentId(): int
    {
        return 3;
    }

    protected function getUserSchema(): array
    {
        return [
            'user 1' => [1, 1, 0, 1, 1, 22, 1],
            'user 2' => [2, 1, 1, 2, 2, 9, 2],
            'user 3' => [3, 1, 2, 3, 3, 8, 3],
            'user 4' => [4, 1, 3, 4, 4, 5, 4],
            'user 5' => [5, 1, 1, 5, 10, 11, 2],
            'user 6' => [6, 1, 1, 6, 12, 21, 2],
            'user 7' => [7, 1, 6, 7, 13, 16, 3],
            'user 8' => [8, 1, 6, 8, 17, 20, 3],
            'user 9' => [9, 1, 8, 9, 18, 19, 4],
            'user 10' => [10, 1, 7, 10, 14, 15, 4],
            'user 11' => [11, 1, 3, 11, 6, 7, 4]
        ] ;
    }

}