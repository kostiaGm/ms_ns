<?php
/**
 * run this test
 * php vendor/bin/phpunit unit/InsertNodeUnderUserId7Test.php
 */

namespace MySQLNestedSetsApiTest\unit;


class InsertNodeUnderUserId6Test extends AbstractInsertTest
{
    /**
     * [1, 2, 3, 4, 5, 6, 7]
     * 1 - id PK, 2 - tree_root, 3 - parent_id, 4 - user_id, 5 - lft, 6 - rgt, 7 - lvl
     * @return array
     */


    /**
     *  -------------------- Before insert
     *
     *                  1      (  1 )     20 (8)
     *                 /             /            \
     *            2 ( 2 ) 7      8 ( 5 ) 9   10 ( 6 ) 19
     *              /                        /        \
     *         3 ( 3 ) 6              11 ( 7 ) 14    15 ( 8 ) 18
     *            /                       /                \
     *       4 ( 4 ) 5             12  ( 10 ) 13          16 ( 9 ) 17
     */

    /**
     *  -------------------- After inserted
     *
     *                  1      (  1 )     22
     *                 /             /            \
     *            2 ( 2 ) 7      8 ( 5 ) 9       10 ( 6 ) 21
     *                                            /        \                \
     *              /                         /              \                \
     *         3 ( 3 ) 6              11 ( 7 ) 14        15 ( 8 ) 18    19 ( 11 new ) 20
     *            /                     /                       \
     *     4 ( 4 ) 5           12 ( 10 ) 13                   16 ( 9 ) 17
     *
     */




    protected function getParentId(): int
    {
        return 6;
    }

    protected function getUserSchema(): array
    {
        return [
            'user 1' => [1, 1, 0, 1, 1, 22, 1],
            'user 2' => [2, 1, 1, 2, 2, 7, 2],
            'user 3' => [3, 1, 2, 3, 3, 6, 3],
            'user 4' => [4, 1, 3, 4, 4, 5, 4],
            'user 5' => [5, 1, 1, 5, 8, 9, 2],
            'user 6' => [6, 1, 1, 6, 10, 21, 2],
            'user 7' => [7, 1, 6, 7, 11, 14, 3],
            'user 8' => [8, 1, 6, 8, 15, 18, 3],
            'user 9' => [9, 1, 8, 9, 16, 17, 4],
            'user 10' => [10, 1, 7, 10, 12, 13, 4],
            'user 11' => [11, 1, 6, 11, 19, 20, 3]
        ];
    }
}