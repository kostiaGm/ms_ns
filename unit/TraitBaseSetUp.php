<?php


namespace MySQLNestedSetsApiTest\unit;


trait TraitBaseSetUp
{
    protected function setUp(): void
    {

        $this->clearData();
        $dataProvider = $this->getDataProvider();

        foreach ($dataProvider as $userName => $row) {

            $userId = $row[3];
            $parentId = ($row[2] !== 0 ? $row[2] : 'NULL');

            try {
                $this->executeSql("call CreateNode($userId, $parentId)");
            } catch (Exception $e) {
                var_dump($e->getMessage());
                die;
            }

        }
    }

    abstract public function getDataProvider(): array;

    abstract public function executeSql(string $sql): void;

}