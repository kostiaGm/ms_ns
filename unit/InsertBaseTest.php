<?php
/**
 * run this test
 * php vendor/bin/phpunit unit/InsertBaseTest.php
 */

namespace MySQLNestedSetsApiTest\unit;


use PHPUnit\Util\Exception;

class InsertBaseTest extends AbstractMySQLNestedSetsApi
{
    use TraitBaseSetUp;
    public function getTableName(): string
    {
        return 'un_';
    }

    public function testAfterInserted()
    {
        $this->diffArrayDataProviderAndDbTable($this->getDataProvider());
    }


}